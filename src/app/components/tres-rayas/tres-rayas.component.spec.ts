import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TresRayasComponent } from './tres-rayas.component';

describe('TresRayasComponent', () => {
  let component: TresRayasComponent;
  let fixture: ComponentFixture<TresRayasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TresRayasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TresRayasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
